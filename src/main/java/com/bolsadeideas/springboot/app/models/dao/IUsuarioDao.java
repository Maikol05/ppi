package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bolsadeideas.springboot.app.models.entity.Usuario;

public interface IUsuarioDao extends JpaRepository<Usuario, Long>{

	@Query(value="SELECT U FROM Usuario U WHERE U.pass = :pass")
	Usuario findOneByPass(@Param(value = "pass") String pass);
	
	@Modifying
	@Query(value = "insert into tbl_usuario(nombre, apellido, email, nombreU,pass,numero_celular,tipo_usuario) "+
				    "values(:name, :lastName, :email, :nameU, :pass,:numCel,:tipoUsuario)",nativeQuery = true)
	void agregarUsuario(@Param(value="name") String name, @Param(value="lastName") String lastname, @Param(value="email") String email,
				@Param(value="nameU") String nameU,@Param(value="pass") String pass,@Param(value="numCel") String numCel,@Param(value="tipoUsuario") boolean tipoUsuario);
	
}
