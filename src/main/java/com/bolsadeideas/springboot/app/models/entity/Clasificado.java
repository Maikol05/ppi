package com.bolsadeideas.springboot.app.models.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table
public class Clasificado {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	private String facultad;
	private String tecnologia;
	private String asignatura;
	private LocalDate fechaRestante;
	
	@Column(name="fecha_limite")
	private LocalDate fechaLimite;
	
	private String nombre;
	
	@Column(name="num_cel")
	private String numCel;
	private String email;
	
	@Column(name="facultad_color")
	private String facultadColor;
	
	@Column(name="tecnologia_color")
	private String tecnologiaColor;
	
	@Column(name="asignatura_color")
	private String asignaturaColor;
	
	@Lob
	@Column(name="area_texto")
	private String areaTexo;
	
	private LocalDate publicacion;
	
    public Clasificado() {
		
    	this.publicacion=LocalDate.now();
		
	}
	
	public Clasificado(String facultad, String tecnologia, String asignatura, LocalDate fechaLimite, String nombre,
			String numCel, String areaTexo) {
		this.facultad = facultad;
		this.tecnologia = tecnologia;
		this.asignatura = asignatura;
		this.fechaLimite = fechaLimite;
		this.nombre = nombre;
		this.numCel = numCel;
		this.areaTexo = areaTexo;
	}


	public String getFacultad() {
		return facultad;
	}
	public void setFacultad(String facultad) {
		this.facultad = facultad;
	}
	public String getTecnologia() {
		return tecnologia;
	}
	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}
	public String getAsignatura() {
		return asignatura;
	}
	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}
	public LocalDate getFechaLimite() {
		return fechaLimite;
	}
	public void setFechaLimite(LocalDate fechaLimite) {
		this.fechaLimite = fechaLimite;
	}
	public String getAreaTexo() {
		return areaTexo;
	}
	public void setAreaTexo(String areaTexo) {
		this.areaTexo = areaTexo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumCel() {
		return numCel;
	}

	public void setNumCel(String numCel) {
		this.numCel = numCel;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacultadColor() {
		return facultadColor;
	}

	public void setFacultadColor(String facultadColor) {
		this.facultadColor = facultadColor;
	}

	public String getTecnologiaColor() {
		return tecnologiaColor;
	}

	public void setTecnologiaColor(String tecnologiaColor) {
		this.tecnologiaColor = tecnologiaColor;
	}

	public String getAsignaturaColor() {
		return asignaturaColor;
	}

	public void setAsignaturaColor(String asignaturaColor) {
		this.asignaturaColor = asignaturaColor;
	}
	
	public LocalDate getFechaRestante() {
		return fechaRestante;
	}
	public void setFechaRestante(LocalDate fechaRestante) {
		this.fechaRestante = fechaRestante;
	}
	public LocalDate getPublicacion() {
		return publicacion;
	}
	public void setPublicacion(LocalDate publicacion) {
		this.publicacion = publicacion;
	}

	public Long getId() {
		return id;
	}
}
