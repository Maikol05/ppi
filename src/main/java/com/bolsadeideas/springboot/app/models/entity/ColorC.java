package com.bolsadeideas.springboot.app.models.entity;

public class ColorC {

	private String[][] facultad;
	private String[][] tecnologia;
	private String[][] asignatura;
	
private void setFacultad() {
	
	facultad = new String[5][2];
	facultad[0][0] = "Ciencias Sociales";
	facultad[1][0] = "Administracion";
	facultad[2][0] = "Ciencias Agrarias";
	facultad[3][0] = "Ingenieria";
	facultad[4][0] = "Audio Visual";
	
	facultad[0][1] = "#008080";
	facultad[1][1] = "#0000FF";
	facultad[2][1] = "#000080";
	facultad[3][1] = "#FF00FF";
	facultad[4][1] = "#800080";
	
	}


	private void setTecnologia() {
		tecnologia = new String[6][2];
		
		tecnologia[0][0] = "Quimica Industrial";
		tecnologia[1][0] = "Tecnologia Agraria";
		tecnologia[2][0] = "Tecnologia Civil";
		tecnologia[3][0] = "Tecnologia T.I.T";
		tecnologia[4][0] = "Tecnolgia Informatica";
		tecnologia[5][0] = "Tec. Audio Visual";
		
		tecnologia[0][1] = "#00FFFF";
		tecnologia[1][1] = "#008000";
		tecnologia[2][1] = "#00FF00";
		tecnologia[3][1] = "#808000";
		tecnologia[4][1] = "#FFFF00";
		tecnologia[5][1] = "#800000";
		
	}


	private void setAsignatura() {
		
		asignatura= new String[7][2];
		asignatura[0][0] = "Matematicas";
		asignatura[1][0] = "Lengua Materna";
		asignatura[2][0] = "Humanidades";
		asignatura[3][0] = "Calculo Integral";
		asignatura[4][0] = "Calculo Diferencial";
		asignatura[5][0] = "Deportes y Arte";
		asignatura[6][0] = "Geometria";
		
		asignatura[0][1] = "#FF0000";
		asignatura[1][1] = "#000000";
		asignatura[2][1] = "#808080";
		asignatura[3][1] = "#C0C0C0";
		asignatura[4][1] = "#FFFFFF";
		asignatura[5][1] = "#CD5C5C";
		asignatura[6][1] = "#FFA07A";
		
	}


public ColorC() {
		
	
	}


public String getColorFacultad(String facultad2) {
	setFacultad();
	for(int i = 0; i < facultad.length;i++) {
		if(facultad[i][0].equals(facultad2)) {
			return facultad[i][1];
		}
	}
	return null;
}


public String getColorTecnologia(String tecnologia2) {
	setTecnologia();
	for(int i = 0; i < tecnologia.length;i++) {
		if(tecnologia[i][0].equals(tecnologia2)) {
			return tecnologia[i][1];
		}
	}
	return null;
}


public String getColorAsignatura(String asignatura2) {
	setAsignatura();
	for(int i = 0; i < asignatura.length;i++) {
		if(asignatura[i][0].equals(asignatura2)) {
			return asignatura[i][1];
		}
	}
	return null;
}
	
}
