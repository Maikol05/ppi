package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Usuario;



public interface IUsuarioService {

	public List<Usuario> findAll();

	public void save(Usuario usuario);
	
	public Usuario findOne(Long p);
	
	public Usuario FindByPass(String pass);
	
	public void delete(Long id);
}
