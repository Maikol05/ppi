package com.bolsadeideas.springboot.app.models.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao2;
import com.bolsadeideas.springboot.app.models.entity.Usuario;

@Service
public class UsuarioServiceImpl implements IUsuarioService {
	
	@Autowired
	private IUsuarioDao usuarioDao;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public void save(Usuario usuario) {
		usuarioDao.agregarUsuario(usuario.getNombre(),usuario.getApellido(),usuario.getEmail(),usuario.getNombreU(),
									usuario.getPass(),usuario.getNumCel(),usuario.getTipoUsuario());
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findOne(Long p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario FindByPass(String pass) {
		
		return usuarioDao.findOneByPass(pass);
		
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}
}
