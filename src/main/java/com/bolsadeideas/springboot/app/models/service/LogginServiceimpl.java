package com.bolsadeideas.springboot.app.models.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.LogginDao;
import com.bolsadeideas.springboot.app.models.entity.Loggin;

@Service
public class LogginServiceimpl implements ILoggin {

	@Autowired
	private LogginDao logginDao;
	
	@Override
	@Transactional
	public void save(Loggin loggin) {
		logginDao.save(loggin);
	}

	@Override
	@Transactional(readOnly=true)
	public Loggin traer(Long id) {
		// TODO Auto-generated method stub
		return logginDao.findById(id).orElse(null);
	}


}
