package com.bolsadeideas.springboot.app.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_usuario")
public class Usuario {

	@Column
	private String nombre;
	
	@Column
	private String apellido;
	
	@Column
	private String nombreU;
	
	@Column
	@Id
	private String email;
	
	@Column
	private String pass;
	
	@Column(name="numero_celular")
	private String numCel;
	
	@Column(name="tipo_usuario")
	private boolean tipoUsuario;
	
	public Usuario() {

	}
	
	

	public Usuario(String nombre, String apellido, String nombreU, String email, String pass, String numCel) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.nombreU = nombreU;
		this.email = email;
		this.pass = pass;
		this.numCel = numCel;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombreU() {
		return nombreU;
	}

	public void setNombreU(String nombreU) {
		this.nombreU = nombreU;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getNumCel() {
		return numCel;
	}

	public void setNumCel(String numCel) {
		this.numCel = numCel;
	}
	
	public void setTipoUsuario(boolean tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	public boolean getTipoUsuario() {
		return this.tipoUsuario;
	}
}
