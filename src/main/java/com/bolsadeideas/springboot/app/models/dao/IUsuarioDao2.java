package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bolsadeideas.springboot.app.models.entity.Usuario;

public interface IUsuarioDao2 extends JpaRepository<Usuario, Long>{

	@Query(value="select u from Usuario u where u.pass = :pass")
	Usuario devolverUsuario(@Param(value="pass")String pass);
}
