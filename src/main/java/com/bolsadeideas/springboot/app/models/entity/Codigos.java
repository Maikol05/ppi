package com.bolsadeideas.springboot.app.models.entity;

public class Codigos {

	private String facultad;
	private String tecnologia;
	private String asignatura;
	private Long id;
	
	public Codigos() {
		
	}

	
	public Codigos(String facultad, String tecnologia, String asignatura, Long id) {
		
		this.facultad = facultad;
		this.tecnologia = tecnologia;
		this.asignatura = asignatura;
		this.id = id;
	}


	public String getFacultad() {
		return facultad;
	}

	public void setFacultad(String facultad) {
		this.facultad = facultad;
	}

	public String getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}

	public String getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	
}
