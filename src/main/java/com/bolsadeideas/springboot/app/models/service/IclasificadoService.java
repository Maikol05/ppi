package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Clasificado;

public interface IclasificadoService {
	
	public List<Clasificado> findAll();

	public void save(Clasificado clasificado);
	
	public Clasificado findOne(Long p);
	
	public Clasificado FindByPass(String pass);
	
	public void delete(Long id);

	public Clasificado getUltimo();

}
