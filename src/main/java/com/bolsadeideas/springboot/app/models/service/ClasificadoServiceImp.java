package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IClasificadoDao;
import com.bolsadeideas.springboot.app.models.entity.Clasificado;

@Service
public class ClasificadoServiceImp implements IclasificadoService{

	@Autowired
	IClasificadoDao clasificadoDao;

	@Override
	@Transactional(readOnly=true)
	public List<Clasificado> findAll() {
		// TODO Auto-generated method stub
		return (List<Clasificado>) clasificadoDao.findAll();
	}

	@Override
	@Transactional
	public void save(Clasificado clasificado) {
		clasificadoDao.save(clasificado);	
	}

	@Override
	@Transactional(readOnly=true)
	public Clasificado findOne(Long p) {
		// TODO Auto-generated method stub
		return clasificadoDao.findById(p).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public Clasificado FindByPass(String pass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=true)
	public Clasificado getUltimo() {
		Long cuenta = clasificadoDao.count();
		return clasificadoDao.findById(cuenta).orElse(null);
	}
	

}
