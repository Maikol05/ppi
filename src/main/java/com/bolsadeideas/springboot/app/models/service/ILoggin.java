package com.bolsadeideas.springboot.app.models.service;

import com.bolsadeideas.springboot.app.models.entity.Loggin;

public interface ILoggin {
	
	public void save(Loggin loggin);
	public Loggin traer(Long id);

}
