package com.bolsadeideas.springboot.app.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Loggin {
	
	@Id
	private Long id = (long) 1;
	private String nombreU;
	private String pass;
	
	@Column(name="tipo_usuario")
	private boolean tipoUsuario;
	
	public Loggin() {
		
	}
	public boolean getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(boolean tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
    public Loggin(String nombreU, String pass) {
		this.nombreU = nombreU;
		this.pass = pass;
	}
	public void setNombreU(String nombreU) {
		this.nombreU = nombreU;
	}
	public String getNombreU() {
		return nombreU;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
}
