package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.bolsadeideas.springboot.app.models.entity.Clasificado;
import com.bolsadeideas.springboot.app.models.entity.Codigos;
import com.bolsadeideas.springboot.app.models.entity.ColorC;
import com.bolsadeideas.springboot.app.models.entity.Loggin;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.ILoggin;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;
import com.bolsadeideas.springboot.app.models.service.IclasificadoService;


@Controller
public class ppi2Controller {
	
	@Autowired
	private IclasificadoService clasificadoService;
	
	@Autowired
	private ILoggin logginService;
	
	@Autowired
	private IUsuarioService ususarioService;

	
	@PostMapping(value="registrar/clasificado")
	public String detalleGuardar(@RequestParam("accion") String accion, Clasificado clasificado, BindingResult result, Model model) {
		Clasificado clasificado2 = new Clasificado();
		
		if(accion.equals("-")) {
			
			return "redirect:/ver";
			
		}else if(result.hasErrors()){
			
		}else {
			Loggin login2 = logginService.traer((long) 1);
			Usuario usuario = ususarioService.FindByPass(login2.getPass());
			clasificado.setNombre(usuario.getNombre());
			clasificado.setNumCel(usuario.getNumCel());
			clasificado.setEmail(usuario.getEmail());
			
			ColorC color = new ColorC();
			clasificado.setFacultadColor(color.getColorFacultad(clasificado.getFacultad()));
			clasificado.setTecnologiaColor(color.getColorTecnologia(clasificado.getTecnologia()));
			clasificado.setAsignaturaColor(color.getColorAsignatura(clasificado.getAsignatura()));			
			
			clasificadoService.save(clasificado);
		}
		
		return "redirect:/ejem";
	}
	@GetMapping(value="/ver/clasificado")
	public String registrarClasificado(Model model) {
		model.addAttribute("titulo","ver clasificado");
		return "mens";
		
	}
	@GetMapping(value="/ver")
	public String verClass2(Model model) {
	
		Clasificado clasificado = null;
		clasificado = clasificadoService.getUltimo();
		//definir variable resta
		model.addAttribute("clasificado",clasificado);

		return "ver";
	}
	@GetMapping(value="/verclass")
	public String verClasificado4(Model model) {
		List<Clasificado> clasificados = new ArrayList<Clasificado>();
		clasificados = clasificadoService.findAll();
		List<Codigos> codigos = new ArrayList<Codigos>();
		
		for(Clasificado clasificado : clasificados) {
			
			codigos.add(new Codigos(clasificado.getFacultadColor(),
									clasificado.getTecnologiaColor(),
									clasificado.getAsignaturaColor(),
									clasificado.getId()));
			
		}
		model.addAttribute("codigos",codigos);
		return "class";
	}
	@GetMapping(value="/mostrar{id}")
	public String mostrarClasificado(@PathVariable(name="id") Long id, Model model) {
		Clasificado clasificado = null;
		clasificado = clasificadoService.findOne(id);
		//definir variable resta
		model.addAttribute("clasificado",clasificado);
		return "ver";
	}
	@GetMapping(value="/codigocolor")
	public String verColorCodigo(Model model) {
		Loggin login = null;
		login = logginService.traer((long) 1);
		model.addAttribute("nombreU",login.getNombreU());
		return "CodigoC";
	}
}
