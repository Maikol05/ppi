package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.bolsadeideas.springboot.app.models.dao.LogginDao;
import com.bolsadeideas.springboot.app.models.entity.Clasificado;
import com.bolsadeideas.springboot.app.models.entity.Loggin;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.ILoggin;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;


@Controller
public class ppiController {
	
	@Autowired
	private IUsuarioService ususarioService;
	
	@Autowired
	private ILoggin logginservice;
	
	
	@GetMapping(value = "/mens/mens")
	public String visualizarClasificado(Model model) {
		model.addAttribute("titulo","visualizar Clasificado");
		return "redirect:/ejemploc";
	}
	@GetMapping(value = "/ejemploc")
	public String visualizarClasificadoe(Model model) {
		model.addAttribute("titulo","visualizar Clasificado");
		return "ejemplo";
	}

	@GetMapping(value = "/loggin")
	public String logginMethod(Model model) {

		Loggin login = new Loggin();
		model.addAttribute("titulo", "Loggin");
		model.addAttribute("loggin", login);

		return "ppi";
	}
	
	@GetMapping(value="/ejem")
	public String visualizarejem(Model model) {
		model.addAttribute("titulo","visualizar clasificado");
		Clasificado clasificado = new Clasificado();
		model.addAttribute("clasificado",clasificado);
		return "registrarclass";
	}
	
	@GetMapping(value="/visualizar/clasificados")
	public String visualizarClasificados(Model model) {
		model.addAttribute("titulo","visualizar clasificado");
		return "visualizarg";
	}
	
	@GetMapping(value="/visualizar/registrar")
	public String visualizarRegistrar(Model model) {
		return "redirect:/ejem";
	}
	
	@GetMapping(value = "/registrarU")
	public String registrarUsuario(Model model) {
		Usuario usuario = new Usuario();
		model.addAttribute("titulo","Registrar Usuario");
		model.addAttribute("usuario",usuario);		
		return "2";
	}
	@GetMapping(value="/bienvenido")
	public String bienvenidoget(Model model) {
		Loggin login = null;
		login = logginservice.traer((long) 1);
		model.addAttribute("nombreU",login.getNombreU());
		return "bienvenido";
	}
	
	@PostMapping(value = "/bienvenido")
	public String bienvenido(@Valid Loggin login, BindingResult result, Model model) {

		Usuario usuario = null;
		boolean tipoUsuario = login.getTipoUsuario();
		usuario = ususarioService.FindByPass(login.getPass());

		if (result.hasErrors()) {
			
			return "loggin";
			
		} else if (usuario == null) {
			model.addAttribute("mensage", "el usuario no se encuentra <br> con la contraseña dada no existe");
			return "ppi";
			
		} else if (!login.getNombreU().equals(usuario.getNombreU())) {
			
			model.addAttribute("mensage", "el nombre de usuario no coincide");
			return "ppi";
			
		}
		if(tipoUsuario != usuario.getTipoUsuario()){
			model.addAttribute("mensage", "el tipo de usuario no coincide");
			return "ppi";
		}
		
		logginservice.save(login);
		
		model.addAttribute("nombreU",usuario.getNombreU());
		return "bienvenido";
	}
	
	@PostMapping(value="/guardarUsuario")
	public String guardarUsuario(@Valid Usuario usuario, BindingResult result, Model model) {
		
		int usuarioCoincidencia = 0;
		
		if (result.hasErrors()) {			
			return "redirect:/2";
			
		} else if (usuario == null) {
			
			usuarioCoincidencia = 1;
			model.addAttribute("mensage", usuarioCoincidencia);
			return "redirect:/registrarU";
			
		}
		ususarioService.save(usuario);
		return "2";
	}
	
}
