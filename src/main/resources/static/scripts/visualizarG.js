function empezar(){
	
	if(!indexedDB in window){
				console.log("no hay soporte");
				return;
			}
			//linea de codigo para eliminar una archivo base de datos,
			//es solo para mostrarla ya que en la siguiente la volvemos a crear
			//indexedDB.deleteDatabase("otro_test4");
			
		   var abrirBase = indexedDB.open("registroUsuarioA",1);
			
			       abrirBase.onupgradeneeded = function(e){
					   
					db2 = e.target.result;
					
			}
				abrirBase.onsuccess = function(e){
					
						                    var db2 = e.target.result;							
											var transaccion = db2.transaction(["registroUsuarioA"],"readonly");
											var almacen = transaccion.objectStore("registroUsuarioA");
																				
											var arregloClas = new Array();
											var solicitud = almacen.openCursor();
											
											solicitud.onsuccess = function(event){
												var cursor = event.target.result;
												
												if(cursor){
													var llenar = {};	
													llenar.facultad = cursor.value.facultad;
											        llenar.tecnologia = cursor.value.tecnologia;
											        llenar.asignatura = cursor.value.asignatura;
													llenar.key = cursor.primaryKey;
													llave = cursor.value.llave;
													arregloClas.push(llenar);
												}
												if(cursor !== null){
													cursor.continue();
												}else{
													
													var facultad = [["Ciencias Sociales","#c2c201"],
													 ["Administracion","#2d5f8a"],
													 ["Ciencias Agrarias", "#c5fe0d"],
													 ["Ingenieria","#927216"],
													 ["Audio Visual","#fddebe"]];
													 
											var tecnologia = [["Quimica Industrial","#FF5733"],
															 ["Costos y Auditorias","#4CB320"],
															 ["Gestion Publica", "#1B4708"],
															 ["Gestion de Empresas","#444708"],
															 ["Agropecuaria","#083747"],
															 ["Instrumentacion I.","#2F0847"],
															 ["Telecomunicaciones","#B9F3CE"],
															 ["Construccion Civil","#E6F3B9"],
															 ["Organizacion Eventos","#EDB9F3"]];
												
											var asignatura = [["Matematicas","#8D5793"],
															 ["Lengua Materna","#00FF2E"],
															 ["Humanidades", "#829E98"],
															 ["Calculo Integral","#009BFF"],
															 ["Calculo Integral","#8495BF"],
															 ["Deportes y Arte","#BFBB84"],
															 ["Geometria","#EC06FF"]];
															 
												for(var i2 = 0; i2<arregloClas.length;i2++){	
													 
													for(i = 0; i<5; i++){
														if(arregloClas[i2].facultad === facultad[i][0]){
															arregloClas[i2].facultad = facultad[i][1];
															break;
														}
													} 
													for(i = 0; i<9; i++){
														if(arregloClas[i2].tecnologia === tecnologia[i][0]){
															arregloClas[i2].tecnologia = tecnologia[i][1];
															break;
														}
													}
													for(i = 0; i<7; i++){
														if(arregloClas[i2].asignatura === asignatura[i][0]){
															arregloClas[i2].asignatura = asignatura[i][1];
															break;
														}
													}
												}								
												
												var i = 0;
												var  temporizador = window.setInterval(function(){
													
													section = document.getElementById("Seccion");
														var img = document.createElement("img");
														img.setAttribute("class","clasif");
														img.setAttribute("src","ppi/clasif.png");
														img.setAttribute("onmouseover","mouseOver(this)");
														img.setAttribute("onmouseout","mouseOut(this)");
														img.setAttribute("onClick","irPagina("+arregloClas[i].key+")");
														section.appendChild(img);
														
														var divcolor = document.createElement("div");
														divcolor.setAttribute("class","colores");
														
														var div1 = document.createElement("div");
														var div2 = document.createElement("div");
														var div3 = document.createElement("div");
														div1.setAttribute("class","guia");
														div2.setAttribute("class","cuadros");
														div3.setAttribute("class","cuadros");
														
														div1.style.backgroundColor = arregloClas[i].facultad;
														div2.style.backgroundColor = arregloClas[i].tecnologia;
														div3.style.backgroundColor = arregloClas[i].asignatura;
														
														divcolor.appendChild(div1);
														divcolor.appendChild(div2);
														divcolor.appendChild(div3);
														
														i++;
														section.appendChild(divcolor);
														if(i === arregloClas.length){
															window.clearInterval(temporizador);
														}
													}, 700);
													db2.close();
												}
											}
				}
				abrirBase.onerror = function(e){
									console.log("update failed!!");
								}
	};
function mouseOver(obj){
	obj.setAttribute("src","ppi/clasifroll.png");
}
function mouseOut(obj){
	obj.setAttribute("src","ppi/clasif.png");
};
function irPagina(key){
		var us = key;
		var nombre = "us";
		 var pagina = "visualizarInde.html?";
		pagina += nombre + "=" + escape(eval(nombre));
		 window.open(pagina, '_blank');
		var i = 0;
	}

window.addEventListener("load",empezar(),false);